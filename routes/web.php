<?php
/*Frontend*/
Route::group(['namespace' => 'Frontend'], function () {
    include(__DIR__.'/Directory/Frontend/frontend.php');
});
/*endFrontend*/


/*Admins*/
Route::group(['prefix' => 'admin', 'as' => 'admin.','namespace' => 'Admin'], function () {
    include(__DIR__.'/Directory/Admin/admin.php');
});
/*endAdmin*/


/*Trainer*/
Route::group(['prefix' => 'trainer', 'as' => 'trainer.','namespace' => 'Trainer'], function () {
    include(__DIR__.'/Directory/Trainer/trainer.php');
});
/*End trainer*/

/*Trainee*/
Route::group(['prefix' => 'trainee', 'as' => 'trainee.','namespace' => 'Trainee'], function () {
    include(__DIR__.'/Directory/Trainee/trainee.php');
});
/*End trainee*/
