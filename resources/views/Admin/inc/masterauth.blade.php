@extends('master')

@section('style')
@yield('admin-style')
@endsection

@section('context')
    @yield('content')
@endsection

@section('script')
@yield('admin-script')
@endsection

@section('title')
Admin | @yield('admin-auth-title')
@endsection
